﻿using System;
using System.Collections.Generic;
using System.IO;

class Program
{
    public static void Main()
    {
        Console.WriteLine("Введите номер файла");
        var data = ReadFile();

        Console.WriteLine("Введите время для проверки");
        var money = GetResult(data, Console.ReadLine());

        Console.WriteLine(money == -1 ? "Файл не корректен" : money);
    }

    // Рассчёт суммы на счёте к нужному времени
    public static int GetResult(List<string[]> data, string date)
    {
        DateTime checkDate = new DateTime();
        if (date != "") checkDate = DateTime.Parse(date, System.Globalization.CultureInfo.InvariantCulture);

        var money = int.Parse(data[0][0]);

        for (var i = 1; i < data.Count; i++)
        {
            if (date != "")
            {
                DateTime operationDate = DateTime.Parse(data[i][0], System.Globalization.CultureInfo.InvariantCulture);
                if (operationDate > checkDate) break;
            }

            var j = i > 2 && data[i][1] == "revert" ? i - 1 : i;

            money += int.Parse(data[j][1]) * (data[j][2] == "in" ? 1 : -1) * (j < i ? -1 : 1);

            if (money < 0) return -1;
        }
        return money;
    }

    // Запись файла в список массивов строк
    public static List<string[]> ReadFile()
    {
        StreamReader reader = SelectFile();
        List<string[]> data = new List<string[]>();
        string line;
        using (reader)
        {
            while ((line = reader.ReadLine()) != null)
            {
                data.Add(line.Split(" | "));
            }
        }
        return SortFile(data);
    }

    // Сортировка списка массивов строк методом пузырька
    public static List<string[]> SortFile(List<string[]> data)
    {
        string[] temp;
        for (int write = 1; write < data.Count; write++)
        {
            for (int sort = 1; sort < data.Count - 1; sort++)
            {
                var first  = DateTime.Parse(data[sort][0],     System.Globalization.CultureInfo.InvariantCulture);
                var second = DateTime.Parse(data[sort + 1][0], System.Globalization.CultureInfo.InvariantCulture);
                if (first > second)
                {
                    temp = data[sort + 1];
                    data[sort + 1] = data[sort];
                    data[sort] = temp;
                }
            }
        }

        return data;
    }

    // Выбор файла по его номеру (в данном решении существуют файлы 1 (корректный) и 2 (не корректный))
    public static StreamReader SelectFile()
    {
        while (true)
        {
            try
            {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "statement" + Console.ReadLine() + ".txt");
                return new StreamReader(path);
            }
            catch (Exception)
            {
                Console.WriteLine("Файл не найден");
            }
        }
    }
}