﻿using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    public static void Main()
    {
        string[] words = { "code", "doce", "ecod", "framer", "frame" };

        var uniqueAnagrams = GetUniqueAnagram(words);

        WriteResult(uniqueAnagrams);
    }

    // Поиск слов, не составляющих анаграмму со словами до них
    public static string[] GetUniqueAnagram(string[] words)
    {
        List<string> uniqueAnagrams = new List<string>();

        for (var i = 0; i < words.Length; i++)
        {
            char[] mainLeters = words[i].ToCharArray();
            Array.Sort(mainLeters);

            bool isAnagram = false;

            for (var j = i - 1; j >= 0; j--)
            {
                char[] compareLeters = words[j].ToCharArray();
                Array.Sort(compareLeters);
                isAnagram = Enumerable.SequenceEqual(mainLeters, compareLeters);
                if (isAnagram) break;
            }

            if (isAnagram) continue;
            uniqueAnagrams.Add(words[i]);
        }

        uniqueAnagrams.Sort();
        return uniqueAnagrams.ToArray();
    }

    // Вывод слов
    public static void WriteResult(string[] uniqueAnagrams)
    {
        foreach (var word in uniqueAnagrams)
        {
            Console.Write(word + " ");
        }

        Console.WriteLine();
    }
}