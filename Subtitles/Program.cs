﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

class Program
{
    // Переменные времени
    public static int seconds = 0;
    public static int appearIndex = 0;
    public static int disappearIndex = 0;

    // Массив с текстом и цветом текста
    public static string[] wordData = new string[] {"", "", "", "", "", "", "", ""};

    // Список массивов со всем содержимым файла
    public static List<string[]> fileData = ReadFile();

    // Таймер срабатывает раз в секунду
    public static Timer time = new Timer(1000);

    // Вызов и настройка таймера
    public static void Main()
    {
        time.Elapsed += UpdateEvent;
        time.Enabled = true;
        time.AutoReset = true;
        time.Start();
        Console.ReadKey();
    }

    // Вызов событий обновления слов и отрисовки при срабатывании таймера
    private static void UpdateEvent(object sender, ElapsedEventArgs args)
    {
        UpdateWordData(true);
        UpdateWordData(false);
        DrawScreen();
    }

    // Обновление слов
    public static void UpdateWordData(bool isAppear)
    {
        int GetTimeIndex() => isAppear ? appearIndex : disappearIndex;

        while (GetTimeIndex() < fileData.Count && seconds == (int)(TimeSpan.Parse(fileData[GetTimeIndex()][isAppear ? 0 : 1])).TotalSeconds / 60)
        {
            int index = 0;
            switch (fileData[GetTimeIndex()][2])
            {
                case "Top": index = 0; break;
                case "Left": index = 1; break;
                case "Right": index = 2; break;
                case "Bottom": index = 3; break;
            }

            if (isAppear)
            {
                wordData[index] = fileData[GetTimeIndex()][4];
                wordData[index + 4] = fileData[GetTimeIndex()][3];
                appearIndex++;
            }
            else
            {
                wordData[index] = "";
                wordData[index + 4] = "White";
                disappearIndex++;
            }
        }
    }

    // Вывод рамки с текстом в консоль
    public static void DrawScreen()
    {
        Console.Clear();
        Console.WriteLine("Seconds: " + seconds++);
        Console.WriteLine("|----------------------------------------|");
        for (var i = 0; i < 4; i++)
        {
            Console.Write("|");
            if (i == 1)
            {
                WriteWord(i);
                WriteWPSString(40 - wordData[i].Length - wordData[i + 1].Length);
                WriteWord(i+1);
                i++;
            }
            else
            {
                WriteWPSString(GetWPSCount(wordData[i], false));
                WriteWord(i);
                WriteWPSString(GetWPSCount(wordData[i], true));
            }
            Console.WriteLine("|");
        }
        Console.WriteLine("|----------------------------------------|");
    }

    // Вывод строки пробелов нужной длины
    public static void WriteWPSString(int count) => Console.Write(string.Concat(Enumerable.Repeat(" ", count)));

    // Получить кол-во пробелов, соответствующих слову 
    public static int GetWPSCount(string word, bool isCeiling)
    {
        double Count = (40 - word.Length) / 2 + (word.Length % 2 == 1 ? 0.5 : 0);
        return (int)(isCeiling ? Math.Ceiling(Count) : Math.Floor(Count));
    }
    
    // Вывод цветного слова в консоль
    public static void WriteWord(int wordIndex)
    {
        ConsoleColor color;
        switch (wordData[wordIndex + 4])
        {
            case "Red":   color = ConsoleColor.Red;   break;
            case "Green": color = ConsoleColor.Green; break;
            case "Blue":  color = ConsoleColor.Blue;  break;
            default:      color = ConsoleColor.White; break;
        }
        Console.ForegroundColor = color;
        Console.Write(wordData[wordIndex]);
        Console.ResetColor();
    }

    // Запись файла в список массивов строк
    public static List<string[]> ReadFile()
    {
        var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "subtitles.txt");
        StreamReader reader = new StreamReader(path);
        List<string[]> data = new List<string[]>();
        string line;
        using (reader)
        {
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Replace(" - ", "|");
                if (!line.Contains(" ["))
                {
                    line = line.Insert(12, "[Bottom, White] ");
                }
                line = line.Replace(" [", "|").Replace("] ", "|").Replace(", ", "|");
                data.Add(line.Split("|"));
            }
        }
        return data;
    }
}